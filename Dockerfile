FROM python:3.10-bookworm

ENV TZ=Europe/Berlin

##### developer / build stage ##################################################

# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt update -y && apt install -y --no-install-recommends \
    git \
    libgl1-mesa-dri \
    libgl1-mesa-dri \
    tzdata \
    re2c \
    rsync \
    ssh-client \
    nano
    
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

RUN apt update -y && apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev 
    
RUN apt update -y
RUN pip install --upgrade pip
COPY requirements.txt /opt/bluesky/requirements.txt
ADD examples /opt/bluesky/examples
WORKDIR /opt/bluesky
RUN pip install -r requirements.txt

RUN git clone https://codebase.helmholtz.cloud/hzb/bluesky/tutorials/tutorial_notebooks.git 

