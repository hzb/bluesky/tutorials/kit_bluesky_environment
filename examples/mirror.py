from ophyd import Device, EpicsMotor, Component as Cpt

class EpicsMirrorSim(Device):

    tx = Cpt(EpicsMotor, 'TX')
    ty = Cpt(EpicsMotor, 'TY')
    tz = Cpt(EpicsMotor, 'TZ')
    rx = Cpt(EpicsMotor, 'RX')
    ry = Cpt(EpicsMotor, 'RY')
    rz = Cpt(EpicsMotor, 'RZ')