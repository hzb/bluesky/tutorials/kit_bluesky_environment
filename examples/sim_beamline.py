# Install Some EPICS devices

from ad_cam import MySimDetector, set_detector
from mirror import EpicsMirrorSim

epics_2d_det = MySimDetector('DEMOBEAMLINE:SIMCAM1:', name='epics_2d_det')
epics_2d_det.wait_for_connection()
set_detector(epics_2d_det)
epics_2d_det.tiff.write_path_template ="/opt/epics/images" 
epics_2d_det.tiff.read_path_template = "/home/bluesky/workshop/sim_beamline_devices/ADSimDetectorInstance/images"

epics_m3 = EpicsMirrorSim('DEMOBEAMLINE:M3:', name = 'm3')
epics_m3.wait_for_connection()

# Install some python simulated devices

from ophyd.sim import SynAxis, SynGauss, Syn2DGauss
from ophyd import Device, Component as Cpt

# A simlated shutter (0 is closed, 1 is open)
shutter = SynAxis(name="shutter", labels={"motors"})

# A simulatd monochromator (takes one second to get anywhere)
mono = SynAxis(name="mono",egu='eV',events_per_move=10, delay =1,  labels={"motors"})

class SampleStage(Device):

    x = Cpt(SynAxis)
    y = Cpt(SynAxis)

sample_stage = SampleStage(name='sample_stage')

# A simulated i0 Detector
alignment_det = Syn2DGauss("alignment_det",
        sample_stage.x,
        "sample_stage_x",
        sample_stage.y,
        "sample_stage_y",
        center=(3, 4.3),
        Imax=10,
        labels={"detectors"},
    )


# A simulated i1 Detector
i1 = SynGauss(
        "i1",
        mono,
        "mono",
        center=5000,
        Imax=1000,
        noise="uniform",
        sigma=1,
        noise_multiplier=0.9,
        labels={"detectors"},
    )

# A simulated i2 Detector
i2 = SynGauss(
        "i2",
        mono,
        "mono",
        center=5000,
        Imax=1000,
        noise="uniform",
        sigma=1,
        noise_multiplier=0.9,
        labels={"detectors"},
    )

# Install the SECoP devices
from RE_config import *
from secop_ophyd.SECoPDevices import SECoPNodeDevice

# Connect to Gas Dosing SEC Node and generate ophyd device tree
gas_dosing =  SECoPNodeDevice.create('localhost','10800',RE.loop)

# Connect to Reactor Cell SEC Node and generate ophyd device tree
reactor_cell =  SECoPNodeDevice.create('localhost','10801',RE.loop)

# Connect to Gas Analysis SEC Node and generate ophyd device tree
gas_analysis = SECoPNodeDevice.create('localhost','10802',RE.loop)  

gas_dosing.class_from_instance()
reactor_cell.class_from_instance()
gas_analysis.class_from_instance()

from genNodeClass import *

gas_dosing:Gas_dosing = gas_dosing
reactor_cell:Reactor_cell = reactor_cell 
gas_analysis:Gas_analysis = gas_analysis