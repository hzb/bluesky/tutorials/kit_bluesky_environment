from abc import ABC, abstractmethod 
from typing import Any 
from secop_ophyd.SECoPDevices import SECoPReadableDevice, SECoPNodeDevice, SECoPMoveableDevice, SECoPCMDDevice 
from ophyd_async.core.signal import SignalRW, SignalR 


class MassSpectrometer(SECoPReadableDevice, ABC):
    status: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    aquire_time: SignalRW
    go_CMD: SECoPCMDDevice

    @abstractmethod 
    def go(self, wait_for_idle: bool = False):
      """generate new spectrum
       argument: None
       result: None"""

class MassflowController(SECoPMoveableDevice, ABC):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    target: SignalRW
    ramp: SignalRW
    gastype: SignalR
    tolerance: SignalR
    stop_CMD: SECoPCMDDevice
    test_cmd_CMD: SECoPCMDDevice

    @abstractmethod 
    def test_cmd(self, arg: dict[str, Any], wait_for_idle: bool = False) -> int:
      """testing with ophyd secop integration
       argument: StructOf(name=StringType(), id=IntRange(0, 1000), sort=BoolType())
       result: IntRange()"""

class PressureController(SECoPMoveableDevice, ABC):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    target: SignalRW
    ramp: SignalRW
    tolerance: SignalR
    stop_CMD: SECoPCMDDevice



class TemperatureController(SECoPMoveableDevice, ABC):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    target: SignalRW
    ramp: SignalRW
    tolerance: SignalR
    stop_CMD: SECoPCMDDevice



class TemperatureSensor(SECoPReadableDevice, ABC):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    heat_flux: SignalRW
    stop_CMD: SECoPCMDDevice



class Gas_analysis(SECoPNodeDevice, ABC):
    equipment_id: SignalR
    firmware: SignalR
    version: SignalR
    description: SignalR
    interface: SignalR
    mass_spec: MassSpectrometer


class Gas_dosing(SECoPNodeDevice, ABC):
    equipment_id: SignalR
    firmware: SignalR
    version: SignalR
    description: SignalR
    interface: SignalR
    massflow_contr1: MassflowController
    massflow_contr2: MassflowController
    massflow_contr3: MassflowController
    backpressure_contr1: PressureController


class Reactor_cell(SECoPNodeDevice, ABC):
    equipment_id: SignalR
    firmware: SignalR
    version: SignalR
    description: SignalR
    interface: SignalR
    temperature_reg: TemperatureController
    temperature_sam: TemperatureSensor


