# Create a simulated environment to work with

from databroker.v2 import temp
from bluesky import RunEngine
from bluesky.preprocessors import run_decorator
from bluesky.callbacks.best_effort import BestEffortCallback
from bluesky.log import config_bluesky_logging
config_bluesky_logging(level='WARNING')

# Create a run engine and a temporary file backed database. Send all the documents from the RE into that database
RE = RunEngine({},call_returns_result=True)
db = temp()
bec = BestEffortCallback()
RE.subscribe(bec)

RE.ignore_callback_exceptions = False
RE.subscribe(db.v1.insert)